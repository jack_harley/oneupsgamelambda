import { downloadFile, removeDownload, uploadFiles } from './../../utils/fileManager';
import { buildVideoFromGif, buildVideoFromImage, ffprobe } from './../../utils/videoProcessor';
import { getUser, getGame, deleteGame, saveGalleryItem, pushToFollowersFeed, sendPushNotification } from './../../api';
import doGameActions from './game';
import { writeFile } from 'fs';
import { config, tempDir } from './../../utils/constants';
import { spawn } from 'child_process';
import _ from 'lodash';

function isGif(str) {
  const re = /(?:\.([^.]+))?$/;
  const ext = re.exec(str)[1];
  return ext == 'gif'
}

function writeInputsFile(localFilePath, rounds) {
	return new Promise(function(resolve, reject) {

    let content = ""
    _.each(rounds, (round, i) => {
      if(i == 0) {
        content = `file 'download${i}.mp4'`
      } else {
        content = `${content} \r\nfile 'download${i}.mp4'`
      }
    })

		writeFile(localFilePath + '/inputs.txt', content, function(err) {
				if(err) reject(err);
				resolve();
		});

	});
}

/**
 * Runs the FFmpeg executable
 *
 * @param {!{log: !function}} logger - The platform logger
 * @param {string} keyPrefix - The prefix for the key (filename minus extension)
 * @returns {Promise}
 */
function concatVideos(logger, gameId) {
	logger.log('Starting FFmpeg');

	const scaleAndCrop = `scale='min(${ config.videoMaxWidth.toString() }\\,iw):-2',crop=iw:${ config.videoMaxHeight.toString() },setsar=1`;
	const scaleAndCropImgLarge = `scale='min(${ config.videoMaxWidth.toString() }\\,iw):-2',crop=iw:${ config.videoMaxHeight.toString() }`;
	const scaleAndCropImgSmall = `scale='min(${ config.imgSmallWidth.toString() }\\,iw):-2',crop=iw:${ config.imgSmallHeight.toString() }`;
	let concat = '';
	concat = concat.substring(0, concat.length - 1);

	return new Promise((resolve, reject) => {

		const args = [
			'-f', 'concat',
			'-i', 'inputs.txt',
			'-c', 'copy',
			`${gameId}.mp4`
		];

		const opts = {
			cwd: tempDir
		};

		const ff = spawn('ffmpeg', args, opts)
			.on('message', msg => logger.log(msg))
			.on('error', reject)
			.on('close', resolve);

	});
}


function downloadFiles(getDownloadStream, logger, rounds) {
  return new Promise(function(resolve, reject) {

    let promises = []

    _.each(rounds, function(round, i) {
      promises.push( downloadFile(getDownloadStream, logger, round.video.uri, tempDir + `/download${i}.mp4`) )
    })

    Promise.all(promises).then((vals) => {
      resolve();
    })

  });
}

function ffpropedownloads(logger, rounds) {
  return new Promise(function(resolve, reject) {
    let promises = []

    _.each(rounds, function(round, i) {
      promises.push(ffprobe(logger, `download${i}.mp4`))
    })

    Promise.all(promises)
      .then(() => { resolve() }, () => { reject() })
  });
}

function removeDownloads(logger, rounds) {
  return new Promise(function(resolve, reject) {

    let promises = []

    _.each(rounds, function(round, i) {
      promises.push(removeDownload(logger, tempDir + `/download${i}.mp4`))
    })

    Promise.all(promises)
      .then(() => { resolve() }, () => { reject() })

  });
}

export default function oneups(library, logger, invocation, game, user, rounds, bucket) {

	downloadFiles(library.getDownloadStream, logger, rounds)
		.then(() => downloadFile(library.getDownloadStream, logger, rounds[0].video.thumb.large.uri, tempDir + `/${game.gameId}_large.png`))
		.then(() => downloadFile(library.getDownloadStream, logger, rounds[0].video.thumb.small.uri, tempDir + `/${game.gameId}_small.png`))
    .then(() => ffpropedownloads(logger, rounds))
		.then(() => writeInputsFile(tempDir, rounds))
		.then(() => concatVideos(logger, game.gameId))
    .then(() => removeDownloads(logger, rounds))
		.then(() => uploadFiles(library.uploadToBucket, logger, game.gameId))
    .then(() => doGameActions(game, user, game.opponent, logger))
		.then((game) => {
      console.log('game', game);
      game.data = JSON.parse(game.data)
			return invocation.callback(null, game)
		})
		.catch(() => {
			return invocation.callback(error)
		});

};
