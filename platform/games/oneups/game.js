import { deleteGame, saveGalleryItem, pushToFollowersFeed, sendPushNotification } from './../../api';
import { config } from './../../utils/constants';
import _ from 'lodash';

let galleryObj = null;

function deleteGames(gameId, userId, opponentId, logger) {
	return Promise
		.all([deleteGame(gameId, userId, logger), deleteGame(gameId, opponentId, logger)]);
}

function createGalleryItem(game, user, logger) {
	galleryObj = preparePayloadForGallery(game, user, logger);
	const messageUsr = 'your game of OneUps with ' + game.opponent.firstName + ' has finished, view the final video!',
	      messageOpp = 'your game of OneUps with ' + user.firstName + ' has finished, view the final video!',
				data = { type: 'gallery.item', category: 'finished', gameId: game.gameId };

	return saveGalleryItem(galleryObj, logger)
						.then(() => pushToFollowersFeed(game.gameId, user._id, game.opponent._id, logger))
						.then(() => sendPushNotification(game.opponent.pushEnabled, game.opponent.pushToken, messageOpp, data, logger))
}

function preparePayloadForGallery(gameObj, user, logger) {

  const data = {
    player1: _.omit(user, ['challengeCount', 'followersCount', 'followingCount', 'gameCount', 'notifCount', 'pushToken', 'pushEnabled']),
    player2: _.omit(gameObj.opponent, ['challengeCount', 'followersCount', 'followingCount', 'gameCount', 'notifCount', 'pushToken', 'pushEnabled']),
    source: {
      video: `https://s3-eu-west-1.amazonaws.com/${config.destinationBucket}/${gameObj.gameId}.mp4`,
      thumb: {
        large: {
          uri: `https://s3-eu-west-1.amazonaws.com/${config.destinationBucket}/${gameObj.gameId}_large.png`
        },
        small: {
          uri: `https://s3-eu-west-1.amazonaws.com/${config.destinationBucket}/${gameObj.gameId}_small.png`
        }
      }
    }
  };

	if(typeof data.player1.avatar == 'string') data.player1.avatar = JSON.parse(data.player1.avatar);
	if(typeof data.player2.avatar == 'string') data.player2.avatar = JSON.parse(data.player2.avatar);


  const payload = {
    title: 'Game of OneUps',
    description: '',
    type: 'oneups',
    data: JSON.stringify(data),
    playerIds: user._id + ' ' + gameObj.opponent._id,
    gameId: gameObj.gameId,
    createdAt: new Date().toISOString(),
    winner: null,
    loser: null,
    commentCount: 0,
    upvoteCount: 0,
    viewCount: 0,
		public: gameObj.public != undefined && gameObj.public == false ? 0 : 1
  };

  return payload
}

export default function doGameActions(game, user, opponent, logger) {
	return new Promise(function(resolve, reject) {
		createGalleryItem(game, user, logger)
			.then(() => {
				return deleteGames(game.gameId, user._id, opponent._id, logger)
			})
			.then(() => {
				resolve(galleryObj)
			})
	});
}
