'use strict';

process.env['NODE_ENV'] = 'production';

import {spawn, execFile} from 'child_process';
import {unlinkSync, createReadStream, createWriteStream, writeFile} from 'fs';
import {createGzip, Z_BEST_COMPRESSION} from 'zlib';
import {join} from 'path';
import {tmpdir} from 'os';

import { oneups } from './games';

/** @type string **/
const tempDir = process.env['TEMP'] || tmpdir();
const config = require(process.env.CONFIG_FILE || './config.json');


/**
 * The main function
 *
 * @param {!{
 *     getDownloadStream: !function,
 *     getFileLocation: !function,
 *     uploadToBucket: !function
 * }} library - The platform library
 * @param {!{log: !function}} logger - The platform logger
 * @param {!{
 *     event: !object,
 *     callback: !function
 * }} invocation - The invocation
 */
export function main(library, logger, invocation) {

	logger.log(`Start`, invocation.event);

	// separate gameId, userId, gameType
	const { game, user, rounds, bucket } = invocation.event;

	oneups(library, logger, invocation, game, user, rounds, bucket)

};
