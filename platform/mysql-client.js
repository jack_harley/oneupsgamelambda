import mysqlclient from 'node-mysql';

const config = require(process.env.CONFIG_FILE || './config.json');

const pool = new mysqlclient.DB({
  connectionLimit: config.db.mysql.connectionLimit, //important
  host: config.db.mysql.host,
	port: 3306,
  user: config.db.mysql.user,
  password: config.db.mysql.password,
  database: config.db.mysql.dbname,
  debug: false
});

export function mysql(queryStr) {

  return new Promise(function(resolve, reject) {

    pool.getConnection(function(err, connection){
      if(err) {
        connection.release();
        reject(err);
      }

      connection.query(queryStr, function(err, rows){
        connection.release();
        if(err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });

    });

  });

};
