import { createWriteStream } from 'fs';
import _ from 'lodash';


/**
 * Downloads the file to the local temp directory
 *
 * @param {!function} downloadFunc - The platform library's download function
 * @param {!{log: !function}} logger - The platform logger
 * @param {{bucket: !string, key: !string}} sourceLocation - The location of the remote file
 * @param {!string} download - The location of the local file
 * @returns {Promise}
 */
export default function downloadFile(downloadFunc, logger, sourceLocation, download) {

	if(typeof sourceLocation == 'string') {
		const parts = _.split(sourceLocation.replace(/(^\w+:|^)\/\//, ''), '/');
		sourceLocation = {};
		sourceLocation.bucket = parts[1];
		sourceLocation.key = _.join(_.drop(parts, 2), '/');
	}

	return new Promise((resolve, reject) => {
		// logger.log(`Starting download: ${sourceLocation.bucket} / ${sourceLocation.key}`);

		downloadFunc(sourceLocation.bucket, sourceLocation.key)
			.on('end', () => {
				logger.log('Download finished: ' + download);
				resolve();
			})
			.on('error', reject)
			.pipe(createWriteStream(download));
	});
}
