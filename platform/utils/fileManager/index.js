import downloadFile from './downloadFile';
import removeDownload from './removeDownload';
import removeFiles from './removeFiles';
import upload from './upload';
import uploadFile from './uploadFile';
import uploadFiles from './uploadFiles';

export {
  downloadFile,
  removeDownload,
  removeFiles,
  upload,
  uploadFile,
  uploadFiles
}
