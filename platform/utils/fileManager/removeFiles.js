import { unlinkSync } from 'fs';

/**
 * Deletes the local output files
 *
 * @param {!{log: !function}} logger - The platform logger
 * @param {!string} filename - The name of the file
 * @param {!Array<string>} rmFiles - The files to remove after the operation is complete
 */
export default function removeFiles(logger, filename, rmFiles) {
	logger.log(`${filename} complete. Deleting now.`);

	return rmFiles
		.forEach(unlinkSync);
}
