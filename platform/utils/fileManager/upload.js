/**
 * Uploads the file
 *
 * @param {!{log: !function}} logger - The platform logger
 * @param {!function} uploadFunc - The function to upload a processed file
 * @param {!module:fs~ReadStream} fileStream - The stream of a processed file
 * @param {!string} bucket - The remote bucket
 * @param {!string} key - The remote key/file path
 * @param {string} encoding - The Content Encoding
 * @param {string} mimeType - The MIME Type of the file
 * @returns {Promise}
 */
export default function upload(logger, uploadFunc, fileStream, bucket, key, encoding, mimeType) {
	logger.log(`Uploading ${mimeType}`);

	return uploadFunc(bucket, key, fileStream, encoding, mimeType);
}
