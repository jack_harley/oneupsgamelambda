import { config } from './../constants';
import uploadFile from './uploadFile';

/**
 * Uploads the output files
 *
 * @param {!function} uploadFunc - The function to upload a processed file
 * @param {!{log: !function}} logger - The platform logger
 * @param {!string} keyPrefix - The prefix for the key (filename minus extension)
 * @returns {Promise}
 */
export default function uploadFiles(uploadFunc, logger, gameId) {
	return Promise
		.all(Object
			.keys(config.format)
			.map(type => uploadFile(uploadFunc, logger, type, gameId))
		);
}
