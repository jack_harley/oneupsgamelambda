import { unlinkSync } from 'fs';

/**
 * Deletes the download file
 *
 * @param {!{log: !function}} logger - The platform logger
 * @param {!string} localFilePath - The location of the local file
 * @returns {Promise<void>}
 */
export default function removeDownload(logger, localFilePath) {

	logger.log('Deleting download file',localFilePath);

	unlinkSync(localFilePath);

	return Promise.resolve();
}
