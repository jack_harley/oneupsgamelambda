import { tempDir, config } from './../constants';
import { join } from 'path';
import { encode } from './../videoProcessor';
import removeFiles from './removeFiles';
import upload from './upload';
/**
 * Transforms, uploads, and deletes an output file
 *
 * @param {!function} uploadFunc - The function to upload a processed file
 * @param {!{log: !function}} logger - The platform logger
 * @param {!string} keyPrefix - The prefix for the key (filename minus extension)
 * @param {!string} type - The output file type, as specified in the configuration
 * @returns {Promise}
 */
export default function uploadFile(uploadFunc, logger, type, gameId) {

	const format = config.format[type];
	const filename = join(tempDir, `${gameId}${format.postfix}.${format.extension}`);
	const rmFiles = [filename];

	return encode(logger, filename, config.gzip, rmFiles)
		.then(fileStream => upload(
			logger,
			uploadFunc,
			fileStream,
			config.destinationBucket,
			gameId + format.postfix + '.' + format.extension,
			config.gzip ? 'gzip' : null,
			format.mimeType
		))
		.then(() => removeFiles(logger, filename, rmFiles));
}
