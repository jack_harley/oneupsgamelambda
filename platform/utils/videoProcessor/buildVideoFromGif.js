import { spawn } from 'child_process';
import { tempDir } from './../constants';

export default function buildVideoFromGif(logger, filepath, output) {
	logger.log('buildVideoFromImage', filepath);

	return new Promise((resolve, reject) => {

		const args = [
			'-i', `${filepath}`,
			'-c:v', 'libx264',
			'-t', '2.5',
			'-r', '30',
			'-pix_fmt', 'yuv420p',
			'-vf', 'scale=640:360',
			'-profile:v', 'baseline',
			`${output}`
		];

		const opts = {
			cwd: tempDir
		};

		spawn('ffmpeg', args, opts)
			.on('message', msg => logger.log(msg))
			.on('error', reject)
			.on('close', resolve);

	});

}
