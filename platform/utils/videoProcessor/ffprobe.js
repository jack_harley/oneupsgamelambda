import { tempDir, config } from './../constants';
import { execFile } from 'child_process';

/**
 * Runs FFprobe and ensures that the input file has a valid stream and meets the maximum duration threshold.
 *
 * @param {!{log: !function}} logger - The platform logger
 * @returns {Promise}
 */
export default function ffprobe(logger, filename) {
	logger.log('Starting FFprobe');

	return new Promise((resolve, reject) => {
		const args = [
			'-v', 'quiet',
			'-print_format', 'json',
			'-show_format',
			'-show_streams',
			'-i', `${filename}`
		];
		const opts = {
			cwd: tempDir
		};
		const cb = (error, stdout) => {
			if (error)
				reject(error);

			logger.log(stdout);

			const outputObj = JSON.parse(stdout);
			const maxDuration = config.videoMaxDuration;

			const hasVideoStream = outputObj.streams.some(stream =>
				stream.codec_type === 'video' &&
				(stream.duration || outputObj.format.duration) <= maxDuration
			);

			if (!hasVideoStream)
				reject('FFprobe: no valid video stream found');
			else {
				logger.log('Valid video stream found. FFprobe finished.');
				resolve();
			}
		};

		execFile('ffprobe', args, opts, cb)
			.on('error', reject);
	});
}
