import {tmpdir} from 'os';
const tempDir = process.env['TEMP'] || tmpdir();
export default tempDir;
