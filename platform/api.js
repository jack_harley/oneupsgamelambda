import { S3, DynamoDB, Endpoint, SNS } from 'aws-sdk';
import _ from 'lodash';
import { mysql } from './mysql-client';
const config    = require(process.env.CONFIG_FILE || './config.json'),
      s3        = new S3(),
      sns       = new SNS(),
      db        = new DynamoDB({ endpoint: new Endpoint(config.db.dynamodb.url) }),
      docClient = new DynamoDB.DocumentClient(db);

export function getUser(userId) {

    const qryStr =  "SELECT * from " + config.db.mysql.tables.user + " WHERE _id='" + userId + "'";
    return new Promise(function(resolve, reject) {
      mysql(qryStr).then(function(profile) {
        resolve(profile['0'])
      }, function(err) {
        console.log('getUser.err', err);
        reject(err)
      });
    });

}

export function getGame(gameId, userId, logger) {

	const params = {
		TableName: config.db.dynamodb.tables.game,
		Key: {
			"userId": userId,
			"gameId": gameId
		}
	};

	return new Promise(function(resolve, reject) {
		docClient.get(params, function(err, data) {
			if(err !== null) {
				reject(err);
			} else {
				resolve(data.Item);
			}
		});
	});

}

function Game(data) {
  this.Item = data;
  this.TableName = config.db.dynamodb.tables.game;
}

export function saveGame(game) {

  const gameObj = new Game(game);

  return new Promise(function(resolve, reject) {
    docClient.put(gameObj, function(err, data) {
      if(err !== null) {
        reject(err);
      } else {
        resolve(gameObj);
      }
    });
  });


}

export function deleteGame(gameId, userId, logger) {
  logger.log('deleteGame');

  const params = {
    TableName: config.db.dynamodb.tables.game,
    Key: {
      userId: userId,
      gameId: gameId
    }
  }

  return new Promise(function(resolve, reject) {
    docClient.delete(params, function(err, data) {
      if(err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });

}

export function saveGalleryItem(payload, logger) {
  logger.log('saveGalleryItem');
  const qryStr =  "INSERT INTO " + config.db.mysql.tables.gallery + " " +
              "(title, description, type, tags, data, playerIds, gameId, createdAt, winner, loser, public, commentCount, upvoteCount, playCount) " +
              "VALUES " +
              "('" +
              (payload.title || '') + "','" +
              (payload.description || '') + "','" +
              payload.type + "','" +
              (payload.tags || []) + "','" +
              payload.data + "','" +
              (payload.playerIds || '') + "','" +
              payload.gameId + "','" +
              new Date().toISOString() + "','" +
              payload.winner + "','" +
              payload.loser + "','" +
              payload.public + "','" +
              0 + "','" +
              0 + "','" +
              0 + "')";

  const gameId = payload.gameId;

  return new Promise(function(resolve, reject) {
    mysql(qryStr).then(function(data) {
      resolve(data)
    }, function(err) {
      reject(err)
    });
  });

}

export function pushToFollowersFeed(gameId, player1Id, player2Id, logger) {
  logger.log('pushToFollowersFeed');

  // get both users Ids
  var followersArrs = {};
  var followers = [];
  var createdAt = new Date().toISOString();
  var params = { RequestItems: {} }
  params.RequestItems[config.db.dynamodb.tables.feed] = [];

  var d = new Date();
  var seconds = Math.round(d.getTime() / 1000);

  var payload = function(userId) {
    return {
      PutRequest: {
        Item: {
          userId: userId,
          createdAt: createdAt,
          expirationTime: seconds + 31622400,
          gameId: gameId
        }
      }
    }
  }

  logger.log('pushToFollowersFeed', payload);

  return new Promise(function(resolve, reject) {
    getFollowers(player1Id)
      .then(function(followers) {
        logger.log('player1Id.followers', followers)
        followersArrs.player1 = _.map(followers, 'followerId');
        return getFollowers(player2Id);
      }, function(err) { deferred.reject(err) })
      .then(function(followers) {
        logger.log('player2Id.followers', followers)
        followersArrs.player2 = _.map(followers, 'followerId');
        // create list of both users followers
        followers = _.union(followersArrs.player1, followersArrs.player2);
        logger.log('followers', followers);

        // add player1 & player2 to list
        followers.push(player1Id, player2Id);
        followers = _.uniq(followers);
        logger.log('followers final', followers);

        // split into chunks of 25 (max no of items for batchWrite)
        var chunks = _.chunk(followers, 25);
        var promises = [];

        _.each(chunks, function(chunk) {

          var chunkParams = _.clone(params);

          _.each(chunk, function(userId) {
            chunkParams.RequestItems[config.db.dynamodb.tables.feed].push(payload(userId));
          });

          promises.push(pushBatchToFeed(params));

        });

        // resolve('done');
        Promise.all(promises)
          .then(() => {
            resolve()
          }, () => {
            reject()
          })

      }, function(err) { deferred.reject(err) })

  });

}

export function sendPushNotification(enabled, endpoint, message, data, logger) {

  if(enabled == 0) {
    return Promise.resolve('true');
  }

  return new Promise(function(resolve, reject) {

    data = JSON.stringify(data)

    let payload = {
      default: message,
      GCM:{
        data:{
          message: message,
          data: data
        }
      },
      APNS: {
        aps: {
          alert: message
        },
        data: data
      },
      APNS_SANDBOX: {
        aps: {
          alert: message
        },
        data: data
      }
    }

    payload.GCM = JSON.stringify(payload.GCM)
    payload.APNS = JSON.stringify(payload.APNS)
    payload.APNS_SANDBOX = JSON.stringify(payload.APNS_SANDBOX)

    var params = {
      Message: JSON.stringify(payload), /* required */
      MessageStructure: 'json',
      TargetArn: endpoint
    };
    sns.publish(params, function(err, data) {
      resolve(data);
    });
  });

}

function pushBatchToFeed(params) {
  return new Promise(function(resolve, reject) {
    docClient.batchWrite(params, function (err, data) {
      if(err) reject(err)
      else resolve(data)
    })
  });
}

function getFollowers(userId) {

  var params = {
    Limit: 10,
    // Table
    TableName: config.db.dynamodb.tables.followers,
    KeyConditionExpression: "#hash = :userId",
    ExpressionAttributeNames: {
        "#hash": 'userId'
    },
    ExpressionAttributeValues: {
        ":userId": userId
    }
  };

  return new Promise(function(resolve, reject) {
    docClient.query(params, function(err, data) {
      if(err !== null) {
        reject(err);
      } else {
        resolve(data.Items);
      }
    });
  });

}
